package xox;

import java.util.Scanner;

public class Game {

	Scanner scan = new Scanner(System.in);
	Form oyun = new Form();

	public void start() {
		boolean isFinished = true;
		System.out.println("Merhaba..XOX oyununa hosgeldiniz.");
		System.out.println(
				"Öncelikle 3x3 bir oyunda ilk sayi satir ikinci sayi sütun olacak sekilde 2 sayi giriniz ve X harfini koyunuz");
		while (isFinished) {
			int sayiSatir = scan.nextInt();
			int sayiSütun = scan.nextInt();
			if (sayiSatir < 3 && sayiSatir >= 0) {
				if (sayiSütun < 3 && sayiSütun >= 0) {
					boolean checkX = oyun.putX(sayiSatir, sayiSütun);
					if (checkX == false) {
						continue;
					}

				}
			} else {
				System.out.println("Sectiginiz sayi 0,1 veya 2 olamli tekrar deneyin.");
				continue;
			}

			System.out.println("-----------------");

			if (hasThreeSymbol("X") || hasThreeSymbol("O")) {
				isFinished = false;
				continue;
			}
			
			checkOver();

			oyun.putO();
			System.out.println("-----------------");
			System.out.println("O harfi koyuldu. tekrar X harfi koymak istediginiz yeri seciniz");
		}

	}

	public boolean checkOver() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (oyun.array[i][j] == null) {
					return true;

				}
			}
		}
		System.out.println("Tüm alanlar dolu oldugu icin oyun berabere bitti");
		return false;
	}

	public boolean hasThreeSymbol(String player) {
		
		// 1 sol köşeden çapraz
		if (player.equals(oyun.array[0][0]) && player.equals(oyun.array[1][1]) && player.equals(oyun.array[2][2])) {
			System.out.println("Oyun bitti.");
			System.out.println(player+" kazandi");
			return true;
		}

		// 2 sağ köşeden çapraz
		if (player.equals(oyun.array[0][2]) && player.equals(oyun.array[1][1]) && player.equals(oyun.array[2][0])) {
			System.out.println("Oyun bitti.");
			System.out.println(player+" kazandi");
			return true;
		}

		// 3 ortadan sütun
		if (player.equals(oyun.array[0][1]) && player.equals(oyun.array[1][1]) && player.equals(oyun.array[2][1])) {
			System.out.println("Oyun bitti.");
			System.out.println(player+" kazandi");
			return true;
		}

		// 4 orta satır
		if (player.equals(oyun.array[1][0]) && player.equals(oyun.array[1][1]) && player.equals(oyun.array[1][2])) {
			System.out.println("Oyun bitti.");
			System.out.println(player+" kazandi");
			return true;
		}

		// 5 sol sütun
		if (player.equals(oyun.array[0][0]) && player.equals(oyun.array[1][0]) && player.equals(oyun.array[2][0])) {
			System.out.println("Oyun bitti.");
			System.out.println(player+" kazandi");
			return true;
		}

		// 6 sağ sütun
		if (player.equals(oyun.array[0][2]) && player.equals(oyun.array[1][2]) && player.equals(oyun.array[2][2])) {
			System.out.println("Oyun bitti.");
			System.out.println(player+" kazandi");
			return true;
		}

		// 7 ilk satır
		if (player.equals(oyun.array[0][0]) && player.equals(oyun.array[0][1]) && player.equals(oyun.array[0][2])) {
			System.out.println("Oyun bitti.");
			System.out.println(player+" kazandi");
			return true;
		}

		// 8 son satır
		if (player.equals(oyun.array[2][0]) && player.equals(oyun.array[2][1]) && player.equals(oyun.array[2][2])) {
			System.out.println("Oyun bitti.");
			System.out.println(player+" kazandi");
			return true;
		}

		return false;
	}

}
