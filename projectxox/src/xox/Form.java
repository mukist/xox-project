package xox;


public class Form {
	
	RandomNumbers rn = new RandomNumbers();

	String[][] array = new String[3][3];

	public void putO() {
		
		rn.randomNumbers();
		
		while (true) {
			
			if (!"O".equals(array[rn.getX()][rn.getY()]) && !"X".equals(array[rn.getX()][rn.getY()])) {
				array[rn.getX()][rn.getY()] = "O";
				break;

			} else {
				rn.randomNumbers();
			}
		}
		screenArray();
	}

	public boolean putX(int sayiRow, int sayiCol) {
		if (array[sayiRow][sayiCol] == null) {
			array[sayiRow][sayiCol] = "X";
		} else {
			System.out.println("Sectiginiz yer dolu tekrar deneyin.");
			return false;
			
		}
		screenArray();
		return true;

	}

	public void screenArray() {
		System.out.println("-----------------");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (array[i][j] != null) {
					System.out.print(array[i][j] + " ");
				} else {
					System.out.print("_" + " ");
				}
			}
			System.out.println();
		}
	}

}
